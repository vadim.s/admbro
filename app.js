const express = require('express');
const AdminBro = require('admin-bro');
const AdminBroSequelize = require('admin-bro-sequelizejs');
const formidableMiddleware = require('express-formidable');
const AdminBroExpressjs = require('admin-bro-expressjs');
const bcrypt = require('bcrypt');
const { usersResourceOptions, postsResourceOptions } = require('./config/resourseOptions');
const db = require('./models');
const ruTranslations = require('./config/ru-translations');

const { User, Post } = db;
require('dotenv').config();

AdminBro.registerAdapter(AdminBroSequelize);

const adminBro = new AdminBro({
  databases: [db],
  rootPath: '/admin',
  resources: [
    { resource: User, options: usersResourceOptions },
    { resource: Post, options: postsResourceOptions },
  ],
  branding: {
    companyName: 'Компания',
  },
  locale: {
    language: 'ru',
    translations: ruTranslations,
  },
});

const app = express();
app.use(formidableMiddleware());

// Routes definitions
app.get('/', (req, res) => res.send('Welcome!<br><a href="/admin">Enter admin panel</a>'));

// Route which returns all users from the database
// app.get('/users', async (req, res) => {
//   const users = await db.User.findAll();
//   res.send(users);
// });

// Route which creates new user
// app.post('/users', async (req, res) => {
//   const details = req.fields;
//   const user = await db.User.create(details);
//   res.send(user);
// });

// Build and use a router which will handle all AdminBro routes
const router = AdminBroExpressjs.buildAuthenticatedRouter(adminBro, {
  authenticate: async (email, password) => {
    const user = await db.User.findOne({ where: { email } });
    if (user) {
      const matched = await bcrypt.compare(password, user.encryptedPassword);
      if (matched) {
        return user;
      }
    }
    return false;
  },
  cookiePassword: process.env.COOKIE_PASS,
}, null, {
  resave: true,
  saveUninitialized: true,
});

app.use(adminBro.options.rootPath, router);

const port = process.env.NODE_ENV === 'production' ? process.env.PORT : 3000;
app.listen(port, () => {
  console.log(`Running on port ${port}`);
});
