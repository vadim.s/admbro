const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      User.hasMany(models.Post, { foreignKey: 'userId' });
    }
  }
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    encryptedPassword: { type: DataTypes.STRING, allowNull: false },
    role: { type: DataTypes.ENUM('admin', 'restricted'), allowNull: false },
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
