const users = [
  {
    firstName: 'Ivan',
    lastName: 'Ivanov',
    email: 'test@example.com',
    role: 'admin',
    encryptedPassword: '$2b$10$1iU9n97lsQRYmZx3gUKv0evbSXQhwcHt1Dky2M4/yFX4cCsBAH2.C',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    firstName: 'Petr',
    lastName: 'Petrov',
    email: 'vs1905@gmail.com',
    role: 'restricted',
    encryptedPassword: '$2b$10$wq/7D7vSyuFDtaC2tHW7m.qH8npHYeis.OtLvMo/lFDCosckmFa0y',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', users, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  },
};
