const faker = require('faker');

const createPosts = (number) => {
  const arr = [];
  for (let i = 0; i < number; i += 1) {
    arr.push({
      subject: faker.lorem.sentence(),
      content: faker.lorem.paragraphs(),
      userId: faker.random.number({ min: 1, max: 2 }),
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }
  return arr;
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Posts', createPosts(20), {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Posts', null, {});
  },
};
