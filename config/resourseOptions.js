const bcrypt = require('bcrypt');

const contentParent = {
  name: 'Ресурсы',
  icon: 'RowExpand',
};

const postsResourceOptions = {
  parent: contentParent,
  properties: {
    content: {
      type: 'richtext',
      custom: {
        toolbar: [['bold', 'italic'], ['link', 'image']],
      },
    },
    userId: {
      isVisible: {
        edit: false,
        show: true,
        list: true,
        filter: true,
      },
    },
  },
  actions: {
    new: {
      before: async (request, { currentAdmin }) => {
        request.payload = {
          ...request.payload,
          userId: currentAdmin.id,
        };
        return request;
      },
    },
    edit: {
      isAccessible: ({ currentAdmin, record }) => currentAdmin && (
        currentAdmin.role === 'admin'
          || currentAdmin.id === record.param('userId')
      ),
    },
    delete: {
      isAccessible: ({ currentAdmin, record }) => currentAdmin && (
        currentAdmin.role === 'admin'
          || currentAdmin.id === record.param('userId')
      ),
    },
  },
};

const usersResourceOptions = {
  parent: contentParent,
  properties: {
    encryptedPassword: {
      isVisible: false,
    },
    password: {
      type: 'string',
      isVisible: {
        list: false,
        edit: true,
        filter: false,
        show: false,
      },
    },
  },
  actions: {
    new: {
      isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
      before: async (request) => {
        if (request.payload.password) {
          request.payload = {
            ...request.payload,
            encryptedPassword: await bcrypt.hash(request.payload.password, 10),
            password: undefined,
          };
        }
        return request;
      },
    },
    edit: {
      isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
    },
    delete: {
      isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
    },
  },
};

module.exports = { postsResourceOptions, usersResourceOptions };
